import React, { Component } from 'react';

class Lottery extends Component {
    array = [5, 6, 7, 8, 9, 10, 11, 12,
        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36
    ];

    state = {
        numbers: this.Shuffle(this.array, 5)
    };
    Shuffle(arr, count) {
        var shuffled = arr.slice(0),
            i = arr.length,
            min = i - count,
            temp, index;
        while (i-- > min) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(min).sort(function(a, b) {
            return a - b;
        });
    }
    changeRandomNumbers = () => {
        this.setState({
            numbers: this.Shuffle(this.array, 5)
        })
    }

    render() {
        return ( <
            div className = "container" >
            <
            button className = "button"
            onClick = { this.changeRandomNumbers } > Change <
            /button> <
            div className = "numbers" >
            <
            div className = "number" >
            <
            p > { this.state.numbers[0] } < /p> <
            /div> <
            div className = "number" >
            <
            p > { this.state.numbers[1] } < /p> <
            /div> <
            div className = "number" >
            <
            p > { this.state.numbers[2] } < /p> <
            /div> <
            div className = "number" >
            <
            p > { this.state.numbers[3] } < /p> <
            /div> <
            div className = "number" >
            <
            p > { this.state.numbers[4] } < /p> <
            /div> <
            /div> <
            /div>
        );
    }
};
export default Lottery;